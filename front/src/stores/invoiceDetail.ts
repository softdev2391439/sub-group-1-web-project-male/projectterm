import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { useLoadingStore } from '@/stores/loading'

import invoiceDetailService from '@/services/invoiceDetail'
import { useMaterialStore } from './material'
import type { Material } from '@/types/Material'
import type { InvoiceDetail } from '@/types/Invoice/InvoiceDetail'

export const useStockDetailStore = defineStore('invoiceDetailStore', () => {
  const loadingStore = useLoadingStore()
  const materialStore = useMaterialStore()

  const dialogDelete = ref(false)
  const dialog = ref(false)
  const loading = ref(false)
  const invoiceId = ref<number>(-1) //invoice id
  const invoiceDetails = ref<InvoiceDetail[]>([])
  const initInvoiceDetail = ref<InvoiceDetail>({
    IND_ID: -1,
    IND_MATERIAL: -1,
    IND_AMOUT: 0,
    IND_PRICE: 0,
    IND_INVOICE: -1
  })
  const editedStockDetail = ref<InvoiceDetail>(JSON.parse(JSON.stringify(initInvoiceDetail.value)))

  //Read
  async function getInvoiceDetails() {
    loadingStore.doLoad()
    const res = await invoiceDetailService.getInvoiceDetails()
    invoiceDetails.value = res.data
    loadingStore.finish()
  }

  //Read
  async function getInvoiceDetailsByInvoiceId(InvoiceId: number) {
    loadingStore.doLoad()
    invoiceId.value = InvoiceId
    const res = await invoiceDetailService.getInvoiceDetailsByInvoiceId(InvoiceId)
    invoiceDetails.value = res.data
    loadingStore.finish()
  }

  //Create or Update
  async function saveInvoiceDetail(invoiceDetail: InvoiceDetail) {
    loadingStore.doLoad()
    if (invoiceDetail.IND_ID < 0) {
      //Add new
      const res = await invoiceDetailService.addInvoiceDetail(invoiceDetail)
    } else {
      //Update
      const res = await invoiceDetailService.updateInvoiceDetail(invoiceDetail)
    }
    await getInvoiceDetailsByInvoiceId(invoiceId.value)
    loadingStore.finish()
  }

  //for edit dialog
  function editInvoiceDetail(invoiceDetail: InvoiceDetail) {
    editedStockDetail.value = Object.assign({}, invoiceDetail)
    dialog.value = true
  }

  //for delete dialog
  function sendToDelete(invoiceDetail: InvoiceDetail) {
    editedStockDetail.value = invoiceDetail
    dialogDelete.value = true
  }

  async function deleteItemConfirm(invoiceDetail: InvoiceDetail) {
    await deleteInvoiceDetail(invoiceDetail)
    closeDelete()
  }

  async function deleteInvoiceDetail(invoiceDetail: InvoiceDetail) {
    loadingStore.doLoad()
    const res = await invoiceDetailService.delInvoiceDetail(invoiceDetail)
    await getInvoiceDetailsByInvoiceId(invoiceId.value)
    invoiceId.value = -1
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedStockDetail.value = initInvoiceDetail.value
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedStockDetail.value = Object.assign({}, initInvoiceDetail.value)
    })
  }
  function getMaterialName(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.name : 'Unknown Material'
  }
  function getMaterialType(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.type : 'Unknown Material'
  }

  function getMinimum(materialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === materialId)
    return material ? material.minimum : null
  }

  return {
    dialogDelete,
    dialog,
    loading,
    invoiceDetails,
    initInvoiceDetail,
    editedStockDetail,
    getInvoiceDetails,
    getInvoiceDetailsByInvoiceId,
    saveInvoiceDetail,
    editInvoiceDetail,
    sendToDelete,
    deleteItemConfirm,
    closeDelete,
    closeDialog,
    getMaterialName,
    getMaterialType,
    getMinimum
  }
})
