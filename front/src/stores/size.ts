import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import sizeService from '@/services/size'
import type { Size } from '@/types/Size'

export const useSizeStore = defineStore('size', () => {
  const loadingStore = useLoadingStore()
  const sizes = ref<Size[]>([])
  const initialSize: Size = {
    name: ''
  }

  const editedSize = ref<Size>(JSON.parse(JSON.stringify(initialSize)))

  async function getSize(id: number) {
    loadingStore.doLoad()
    const res = await sizeService.getSize(id)
    editedSize.value = res.data
    loadingStore.finish()
  }

  async function getSizes() {
    try {
      loadingStore.doLoad()
      const res = await sizeService.getSizes()
      sizes.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveSize() {
    loadingStore.doLoad()
    const size = editedSize.value
    if (!size.id) {
      // Add new
      console.log('Post ' + JSON.stringify(size))
      const res = await sizeService.addSize(size)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(size))
      const res = await sizeService.updateSize(size)
    }

    await getSizes()
    loadingStore.finish()
  }
  async function deleteSize() {
    loadingStore.doLoad()
    const size = editedSize.value
    const res = await sizeService.delSize(size)

    await getSizes()
    loadingStore.finish()
  }

  function clearForm() {
    editedSize.value = JSON.parse(JSON.stringify(initialSize))
  }
  return { sizes, getSizes, saveSize, deleteSize, editedSize, getSize, clearForm }
})
