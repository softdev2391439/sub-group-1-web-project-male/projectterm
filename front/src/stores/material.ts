import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import materialService from '@/services/material'
import { useLoadingStore } from '@/stores/loading'

import type { Material } from '@/types/Material'
export const useMaterialStore = defineStore('materialStore', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const loadingStore = useLoadingStore()

  const initMaterial: Material = {
    id: -1,
    name: '',
    type: '',
    minimum: 0,
    amount: 0
  }
  const initrefMaterial = ref<Material[]>([])

  const editMaterial = ref<Material>(JSON.parse(JSON.stringify(initMaterial)))
  const materials = ref<Material[]>([])
  const dbmaterials = ref<Material[]>([])
  const addsomeMaterial = ref<Material[]>([])
  const amount = ref(0)

  let editedIndex = -1

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editMaterial.value = Object.assign({}, initMaterial)
    })
  }

  function deleteItemConfirm() {
    materials.value.splice(editedIndex, 1)
    closeDelete()
  }

  function editItem(item: Material) {
    editedIndex = materials.value.indexOf(item)
    editMaterial.value = Object.assign({}, item)
    dialog.value = true
  }

  function deleteItem(item: Material) {
    removeMatetial(item)
    editMaterial.value = Object.assign({}, item)
    dialogDelete.value = true
  }

  async function initialize() {
    const res = await materialService.getMaterials()
    materials.value = res.data
  }

  async function getMaterials() {
    loadingStore.doLoad()
    const res = await materialService.getMaterials()
    const materialStore = res.data.map((material: any) => {
      return {
        id: material.M_ID,
        name: material.M_NAME,
        minimum: material.M_MIN,
        type: material.M_TYPE,
        amount: material.M_AMOUNT
      }
    })
    materials.value = materialStore
    loadingStore.finish()
  }

  async function getMaterialsInNewAtt() {
    loadingStore.doLoad()
    const res = await materialService.getMaterials()
    const materialStore = res.data.map((material: any) => {
      return {
        id: material.M_ID,
        name: material.M_NAME,
        minimum: material.M_MIN,
        type: material.M_TYPE,
        amount: material.M_AMOUNT
      }
    })
    // ไม่คืนค่าเป็น Promise แต่คืนค่าโดยตรง
    dbmaterials.value = materialStore
    loadingStore.finish()
  }

  async function removeMatetial(material: Material) {
    loadingStore.doLoad()
    const res = await materialService.delMaterial(material)
    getMaterials()
    loadingStore.finish()
  }
  function closeDialog() {
    dialog.value = false
    dialogDelete.value = false
    nextTick(() => {
      editMaterial.value = Object.assign({}, initMaterial)
    })
  }

  async function saveMaterial(material: Material) {
    loadingStore.doLoad()
    if (material.id < 0) {
      //Add new
      const res = await materialService.addMaterial(material)
    } else {
      //Update
      const res = await materialService.updateMaterial(material)
    }
    getMaterials()
    loadingStore.finish()
  }

  return {
    materials,
    dialog,
    loading,
    dialogDelete,
    initMaterial,
    editMaterial,
    editedIndex,
    initrefMaterial,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDelete,
    initialize,
    getMaterials,
    getMaterialsInNewAtt,
    dbmaterials,
    closeDialog,
    addsomeMaterial,
    saveMaterial,
    amount,
    removeMatetial
  }
})
