import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'

export const useSalaryStore = defineStore('salary', () => {
  const salaries = ref<Salary[]>([
    {
      id: 1,
      date: new Date('2023-12-15'),
      detail: [
        {
          id: 1,
          name: 'กฤติน ศรสุข',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 2,
          name: 'สุดใจ มีความสุข',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 3,
          name: 'รพีพัฒน์ ดีใจ',
          paymentDate: new Date('2023-12-16'),
          status: 'PAID'
        },
        {
          id: 4,
          name: 'ณัฐฐิชา พุฒิพงษ์',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 5,
          name: 'ธีระพล ประดิษฐ์',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 6,
          name: 'อรวรรณ สุขสวัสดิ์',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 7,
          name: 'พิชาธิป ช่างมัน',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 8,
          name: 'เจนนิ เดนเวอร์',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 9,
          name: 'เอ็มมา สวัสดิ์ดี',
          paymentDate: new Date('2023-12-16'),
          status: 'PAID'
        },
        {
          id: 10,
          name: 'นัทธิพร วงศ์วารี',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 11,
          name: 'ไอริส แซนเจอร์',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 12,
          name: 'โบว์ เวิร์ธ',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 13,
          name: 'แอลเล็กซ์ ซันเดอร์',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        },
        {
          id: 14,
          name: 'โมจิ มาซากิ',
          paymentDate: new Date('2023-12-16'),
          status: 'PAID'
        },
        {
          id: 15,
          name: 'แชร์ล็อต สตีเวนสัน',
          paymentDate: new Date('2023-12-15'),
          status: 'PAID'
        }
      ]
    },
    {
      id: 2,
      date: new Date('2024-01-15'),
      detail: [
        {
          id: 1,
          name: 'กฤติน ศรสุข',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 2,
          name: 'สุดใจ มีความสุข',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 3,
          name: 'รพีพัฒน์ ดีใจ',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 4,
          name: 'ณัฐฐิชา พุฒิพงษ์',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 5,
          name: 'ธีระพล ประดิษฐ์',
          paymentDate: new Date('2024-01-16'),
          status: 'PAID'
        },
        {
          id: 6,
          name: 'อรวรรณ สุขสวัสดิ์',
          paymentDate: new Date('2024-01-17'),
          status: 'PAID'
        },
        {
          id: 7,
          name: 'พิชาธิป ช่างมัน',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 8,
          name: 'เจนนิ เดนเวอร์',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 9,
          name: 'เอ็มมา สวัสดิ์ดี',
          paymentDate: new Date('2024-01-16'),
          status: 'PAID'
        },
        {
          id: 10,
          name: 'นัทธิพร วงศ์วารี',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 11,
          name: 'ไอริส แซนเจอร์',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        },
        {
          id: 12,
          name: 'โบว์ เวิร์ธ',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 13,
          name: 'แอลเล็กซ์ ซันเดอร์',
          paymentDate: new Date('2024-01-16'),
          status: 'PAID'
        },
        {
          id: 14,
          name: 'โมจิ มาซากิ',
          paymentDate: new Date('2024-01-17'),
          status: 'PAID'
        },
        {
          id: 15,
          name: 'แชร์ล็อต สตีเวนสัน',
          paymentDate: new Date('2024-01-15'),
          status: 'PAID'
        }
      ]
    },
    {
      id: 3,
      date: new Date('2024-02-15'),
      detail: [
        {
          id: 1,
          name: 'กฤติน ศรสุข',
          paymentDate: new Date('2024-02-15 10:01:02'),
          status: 'PAID'
        },
        {
          id: 2,
          name: 'สุดใจ มีความสุข',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 3,
          name: 'รพีพัฒน์ ดีใจ',
          paymentDate: new Date('2024-02-15 10:03:07'),
          status: 'PAID'
        },
        {
          id: 4,
          name: 'ณัฐฐิชา พุฒิพงษ์',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 5,
          name: 'ธีระพล ประดิษฐ์',
          paymentDate: new Date('2024-02-15 10:04:59'),
          status: 'PAID'
        },
        {
          id: 6,
          name: 'อรวรรณ สุขสวัสดิ์',
          paymentDate: new Date('2024-02-16 10:13:04'),
          status: 'PAID'
        },
        {
          id: 7,
          name: 'พิชาธิป ช่างมัน',
          paymentDate: new Date('2024-02-15 10:06:06'),
          status: 'PAID'
        },
        {
          id: 8,
          name: 'เจนนิ เดนเวอร์',
          paymentDate: new Date('2024-02-17 10:08:01'),
          status: 'PAID'
        },
        {
          id: 9,
          name: 'เอ็มมา สวัสดิ์ดี',
          paymentDate: new Date('2024-02-15 10:10:00'),
          status: 'PAID'
        },
        {
          id: 10,
          name: 'นัทธิพร วงศ์วารี',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 11,
          name: 'ไอริส แซนเจอร์',
          paymentDate: new Date('2024-02-15 10:12:48'),
          status: 'PAID'
        },
        {
          id: 12,
          name: 'โบว์ เวิร์ธ',
          paymentDate: new Date('2024-02-18 10:05:03'),
          status: 'PAID'
        },
        {
          id: 13,
          name: 'แอลเล็กซ์ ซันเดอร์',
          paymentDate: new Date('2024-02-15 10:12:15'),
          status: 'PAID'
        },
        {
          id: 14,
          name: 'โมจิ มาซากิ',
          paymentDate: null,
          status: 'NOT PAID'
        },
        {
          id: 15,
          name: 'แชร์ล็อต สตีเวนสัน',
          paymentDate: new Date('2024-02-15 10:13:04'),
          status: 'PAID'
        }
      ]
    }
  ])
  const formatDate = (date: Date | null) =>
    date !== null ? new Date(date).toLocaleDateString() : null
  function formatDateTime(inputDate: Date | null) {
    if (inputDate) {
      const date = new Date(inputDate)
      const formattedDate = date.toLocaleString('en-US', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      })
      return formattedDate
    }
  }

  return { salaries, formatDate, formatDateTime }
})
