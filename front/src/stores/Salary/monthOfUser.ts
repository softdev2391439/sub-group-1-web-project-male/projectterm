import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { SalUser, Salary } from '@/types/Salary/Salary'
import type { CheckDate } from '@/types/CheckInOut/CheckInOutDate'
import { da } from 'vuetify/locale'
import type { mou } from '@/types/Salary/Mou'

export const useMonthOfUserStore = defineStore('monthOfUser', () => {
  function newDialog(
    id: number,
    name: string,
    date: Date,
    timeIn: string,
    timeOut: string,
    totalMin: number
  ): Ref<mou> {
    const dialog = ref<mou>({
      id: 1,
      name: '',
      date: new Date(),
      timeIn: '',
      timeOut: '',
      totalMin: 0
    })
    dialog.value.id = id
    dialog.value.name = name
    dialog.value.date = date
    dialog.value.timeIn = timeIn
    dialog.value.timeOut = timeOut
    dialog.value.totalMin = totalMin
    return dialog
  }

  const allMins = ref<number>(0)

  function WHOTU(months: CheckDate[], month: Salary | undefined, user: SalUser | null): mou[] {
    const dialog: mou[] = []
    if (month && user) {
      for (const day of months) {
        if (
          day.date.getMonth() === month.date.getMonth() &&
          day.date.getFullYear() === month.date.getFullYear()
        ) {
          for (const history of day.checkUser) {
            if (history.id === user.id) {
              const newdialog = newDialog(
                user.id,
                user.name,
                day.date,
                history.timeIn,
                history.timeOut,
                calculateTotalMinutes(history.timeIn, history.timeOut)
              )
              allMins.value = allMins.value + newdialog.value.totalMin
              dialog.push(newdialog.value)
            }
          }
        }
      }
    }
    return dialog
  }

  function calculateTotalMinutes(timeIn: string, timeOut: string): number {
    const convertToMinutes = (time: string): number => {
      const [hours, minutes] = time.split(':').map(Number)
      return hours * 60 + minutes
    }

    return convertToMinutes(timeOut) - convertToMinutes(timeIn)
  }

  function formatMinutesToHHMM(totalMinutes: number): string {
    const hours = Math.floor(totalMinutes / 60)
    const minutes = totalMinutes % 60
    return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}`
  }

  return { WHOTU, calculateTotalMinutes, formatMinutesToHHMM, allMins }
})
