import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import categoryService from '@/services/category'
import type { Category } from '@/types/Category'

export const useCategoryStore = defineStore('category', () => {
  const loadingStore = useLoadingStore()
  const categorys = ref<Category[]>([])
  const initialCategory: Category = {
    name: ''
  }

  const editedCategory = ref<Category>(JSON.parse(JSON.stringify(initialCategory)))

  async function getCategory(id: number) {
    loadingStore.doLoad()
    const res = await categoryService.getCategory(id)
    editedCategory.value = res.data
    loadingStore.finish()
  }

  async function getCategorys() {
    try {
      loadingStore.doLoad()
      const res = await categoryService.getCategorys()
      categorys.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveCategory() {
    loadingStore.doLoad()
    const category = editedCategory.value
    if (!category.id) {
      // Add new
      console.log('Post ' + JSON.stringify(category))
      const res = await categoryService.addCategory(category)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(category))
      const res = await categoryService.updateCategory(category)
    }

    await getCategorys()
    loadingStore.finish()
  }
  async function deleteCategory() {
    loadingStore.doLoad()
    const category = editedCategory.value
    const res = await categoryService.delCategory(category)

    await getCategorys()
    loadingStore.finish()
  }

  function clearForm() {
    editedCategory.value = JSON.parse(JSON.stringify(initialCategory))
  }
  return {
    categorys,
    getCategorys,
    saveCategory,
    deleteCategory,
    editedCategory,
    getCategory,
    clearForm
  }
})
