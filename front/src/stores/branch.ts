import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branches = ref<Branch[]>([])
  const initialBranch: Branch = {
    name: '',
    address: '180 หมู่ 2 ต.บางแสน',
    city: 'บางแสน',
    province: 'ชลบุรี',
    country: 'ไทย',
    postalcode: '20131',
    latitude: 13.281264959275443,
    longitude: 100.92410270978652
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }
  async function getBranches() {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranches()
      branches.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }
  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }

    await getBranches()
    loadingStore.finish()
  }
  async function deleteBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)

    await getBranches()
    loadingStore.finish()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return { branches, getBranches, saveBranch, deleteBranch, editedBranch, getBranch, clearForm }
})
