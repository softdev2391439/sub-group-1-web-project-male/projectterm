import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import service
import utilityCostService from '@/services/utilityCost'
//import store
import { useUserStore } from '../user'

//type
import type { UtilityDetail } from '../../types/utiliycost/UtilityDetail'
import type { UtilityCost } from '../../types/utiliycost/UtilityCost'
import { useLoadingStore } from '../loading'

const userStore = useUserStore()
const price = ref(0)
const type = ref('')
const date = ref(new Date())
const newUcDialog = ref(false)

export const useUtilityStore = defineStore('utility ', () => {
  const loadingStore = useLoadingStore()
  const initUtilityCost = ref<UtilityCost>({
    creatDate: new Date(),
    totalPrice: 0,
    totalItem: 0,
    user: userStore.currentUser,
    ucDetails: []
  })

  const utilityBills = ref<UtilityCost[]>([])

  const ucDetails = ref<UtilityDetail[]>([])

  const newItem = (uc: UtilityDetail) => {
    console.log(uc)
    const index = ucDetails.value.findIndex((item) => item.id === uc.id)
    console.log(index)
    if (index >= 0) {
      return
    } else {
      const newItem: UtilityDetail = {
        id: ucDetails.value.length + 1,
        type: type.value,
        price: price.value,
        date: date.value
      }
      nextTick(() => {
        console.log(newItem)
        ucDetails.value.push(newItem)
        calBill()
        price.value = 0
        type.value = ''
      })
    }
  }

  function calBill() {
    let total = 0
    let totalItem = 0
    for (const item of ucDetails.value) {
      console.log(item.price)
      total = Number(total) + Number(item.price)
      totalItem = ucDetails.value.length
    }
    initUtilityCost.value.totalItem = totalItem
    initUtilityCost.value.totalPrice = total
    initUtilityCost.value.ucDetails = ucDetails.value
  }

  async function getUtilityBill(id: number) {
    loadingStore.doLoad()
    const res = await utilityCostService.getUtilityCost(id)
    initUtilityCost.value = res.data
    loadingStore.finish()
  }
  async function getUtilityBills() {
    try {
      loadingStore.doLoad()
      const res = await utilityCostService.getUtilityCostes()
      utilityBills.value = res.data
      console.log('getBill', utilityBills.value)
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  async function saveBill() {
    loadingStore.doLoad()
    const bill = initUtilityCost.value
    if (!bill.id) {
      // Add new
      console.log('Post ' + JSON.stringify(bill))
      const res = await utilityCostService.addUtilityCost(bill)
    }
    await getUtilityBills()
    loadingStore.finish()
    clear()
  }

  async function deleteBill() {
    loadingStore.doLoad()
    const uc = initUtilityCost.value
    const res = await utilityCostService.delUtilityCost(uc)

    await getUtilityBills()
    loadingStore.finish()
    clear()
  }

  const deleteItem = (m: UtilityDetail) => {
    console.log('item', m)
    const index = ucDetails.value.findIndex((item) => item.id === m.id)
    console.log(
      'Item  that find ',
      ucDetails.value.find((item) => item.id === item.id)
    )
    console.log('Index', index)
    ucDetails.value.splice(index, 1)
    calBill()
  }

  function clear() {
    ucDetails.value = []
    initUtilityCost.value = {
      id: 0,
      creatDate: new Date(),
      totalPrice: 0,
      totalItem: 0,
      user: userStore.currentUser,
      ucDetails: []
    }
    newUcDialog.value = false
  }

  return {
    initUtilityCost,
    utilityBills,
    ucDetails,
    newItem,
    deleteItem,
    calBill,
    saveBill,
    clear,
    type,
    price,
    newUcDialog,
    date,
    getUtilityBills,
    getUtilityBill,
    deleteBill
  }
})
