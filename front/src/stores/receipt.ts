import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useUserStore } from './user'
import { useCustomerStore } from './customer'
import { useLoadingStore } from './loading'
import receiptService from '@/services/receipt'
import type { SaveReceipt } from '@/types/SaveReceipt'


export const useReceiptStore = defineStore('receipt', () => {
  const userStore = useUserStore()
  const memberStore = useCustomerStore()
  const receiptDialog = ref(false)
  const paymentDialog = ref(false)
  const loadingStore = useLoadingStore()
  const CreditcardBGC = ref('rgb(226, 223, 223)')
  const CashBGC = ref('rgb(226, 223, 223)')
  const QRcodeBGC = ref('rgb(226, 223, 223)')
  const CreditcardFC = ref('')
  const CashFC = ref('')
  const QRcodeFC = ref('')
  const receipt = ref<Receipt>({
    totalBefore: 0,
    customerDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    PaymentType: '',
    userId: userStore.currentUser?.id,
    user: userStore.currentUser?.user,
    memberId: 0,
    PromotionDiscount: 0,
    PointDiscount: 0,
    AllDiscount: 0,
    receiptItems: []
  })
  const receiptItems = ref<ReceiptItem[]>([])
  watch(receipt.value.receivedAmount, (newX: any) => {
    calRecAmount()
  })

  async function saveReceipt() {
    try {
      loadingStore.doLoad()
      const formReceipt = <SaveReceipt>({
        totalBefore: String(receipt.value.totalBefore),
        customerDiscount: String(receipt.value.customerDiscount),
        total: String(receipt.value.total),
        receivedAmount: String(receipt.value.receivedAmount),
        change: String(receipt.value.change),
        PaymentType: String(receipt.value.PaymentType),
        user: userStore.currentUser?.user,
        PromotionDiscount: String(receipt.value.PromotionDiscount),
        PointDiscount: String(receipt.value.PointDiscount),
        AllDiscount: String(receipt.value.AllDiscount),
      })
      const saveReciptItem = receiptItems.value

      console.log('Post ' + JSON.stringify(formReceipt))
      // const res = await receiptService.addReceipt(formReceipt, saveReciptItem);

      loadingStore.finish()

    }
    catch (e: any) {
      loadingStore.finish()
    }

  }

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex(
      (item) =>
        item.products!.id === product.id &&
        item.products!.subCategory?.id === product.subCategory?.id &&
        item.products!.size?.id === product.size?.id
    )
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceiptItem: ReceiptItem = {
        id: -1,
        name: product.name,
        price: parseFloat(product.price),
        unit: 1,
        productsId: product.id,
        products: product
      }
      receiptItems.value.push(newReceiptItem)
      calReceipt()
    }
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit == 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }

    receipt.value.totalBefore = totalBefore
    if (memberStore.currentCustomer) {
      receipt.value.customerDiscount = receipt.value.totalBefore * 0.05
      receipt.value.total = receipt.value.totalBefore - receipt.value.customerDiscount
      receipt.value.total = receipt.value.total - receipt.value.PromotionDiscount
      receipt.value.total = receipt.value.total - receipt.value.PointDiscount
      if (receipt.value.total <= 0) {
        receipt.value.total = 0
      }
    } else {
      receipt.value.total = receipt.value.totalBefore - receipt.value.PromotionDiscount
      if (receipt.value.total <= 0) {
        receipt.value.total = 0
      }
    }
  }
  function showReceiptDialog() {
    if (receipt.value.PaymentType == 'Cash') {
      calRecAmount()
    } else {
      receipt.value.change = 0
    }
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }

  function showPaymentDialog() {

    paymentDialog.value = true
  }

  function calRecAmount() {
    receipt.value.change = receipt.value.receivedAmount - receipt.value.total

  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      customerDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      PaymentType: '',
      userId: userStore.currentUser?.id,
      user: userStore.currentUser?.user,
      memberId: 0,
      PromotionDiscount: 0,
      PointDiscount: 0,
      AllDiscount: 0
    }
    memberStore.clear()
    resetColor()
    console.log(receipt.value.totalBefore)
  }

  function closePaymentDialog() {

    paymentDialog.value = false
  }



  function resetColor() {
    CreditcardBGC.value = 'rgb(226, 223, 223)'
    CashBGC.value = 'rgb(226, 223, 223)'
    QRcodeBGC.value = 'rgb(226, 223, 223)'
    CreditcardFC.value = ''
    CashFC.value = ''
    QRcodeFC.value = ''
  }

  const selectPaymentType = (paymentType: string) => {
    receipt.value.PaymentType = paymentType
    resetColor()
    if (paymentType === 'Creditcard') {
      CreditcardBGC.value = 'white'
      CreditcardFC.value = 'rgb(65, 79, 236)'
    } else if (paymentType === 'Cash') {
      CashBGC.value = 'white'
      CashFC.value = 'rgb(65, 79, 236)'
    } else if (paymentType === 'QRcode') {
      QRcodeBGC.value = 'white'
      QRcodeFC.value = 'rgb(65, 79, 236)'
    }
  }

  return {
    dec,
    inc,
    removeReceiptItem,
    addReceiptItem,
    calReceipt,
    showReceiptDialog,
    clear,
    receiptDialog,
    receiptItems,
    receipt,
    saveReceipt,
    showPaymentDialog,
    paymentDialog,
    closePaymentDialog,
    selectPaymentType,
    resetColor,
    CreditcardBGC,
    CashBGC,
    QRcodeBGC,
    CreditcardFC,
    CashFC,
    QRcodeFC,
    calRecAmount
  }
})
