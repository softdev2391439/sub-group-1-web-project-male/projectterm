import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import subCategoryService from '@/services/subCategory'
import type { SubCategory } from '@/types/SubCategory'

export const useSubCategoryStore = defineStore('subCategory', () => {
  const loadingStore = useLoadingStore()
  const subCategorys = ref<SubCategory[]>([])
  const initialSubCategory: SubCategory = {
    name: ''
  }

  const editedSubCategory = ref<SubCategory>(JSON.parse(JSON.stringify(initialSubCategory)))

  async function getSubCategory(id: number) {
    loadingStore.doLoad()
    const res = await subCategoryService.getSubCategory(id)
    editedSubCategory.value = res.data
    loadingStore.finish()
  }

  async function getSubCategorys() {
    try {
      loadingStore.doLoad()
      const res = await subCategoryService.getSubCategorys()
      subCategorys.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  async function saveSubCategory() {
    loadingStore.doLoad()
    const subCategory = editedSubCategory.value
    if (!subCategory.id) {
      // Add new
      console.log('Post ' + JSON.stringify(subCategory))
      const res = await subCategoryService.addSubCategory(subCategory)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(subCategory))
      const res = await subCategoryService.updateSubCategory(subCategory)
    }

    await getSubCategorys()
    loadingStore.finish()
  }
  async function deleteSubCategory() {
    loadingStore.doLoad()
    const subCategory = editedSubCategory.value
    const res = await subCategoryService.delSubCategory(subCategory)

    await getSubCategorys()
    loadingStore.finish()
  }

  function clearForm() {
    editedSubCategory.value = JSON.parse(JSON.stringify(initialSubCategory))
  }
  return {
    subCategorys,
    getSubCategorys,
    saveSubCategory,
    deleteSubCategory,
    editedSubCategory,
    getSubCategory,
    clearForm
  }
})
