import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { useLoadingStore } from '@/stores/loading'

import type { StockDetail } from '@/types/StockDetail'
import type { StockDetailForSend } from '@/types/StockDetailAddnew'

import stockDetailService from '@/services/stockDetail'
import { useMaterialStore } from './material'
import type { Material } from '@/types/Material'

export const useStockDetailStore = defineStore('stockDetail', () => {
  const loadingStore = useLoadingStore()
  const materialStore = useMaterialStore()

  const dialogDelete = ref(false)
  const dialog = ref(false)
  const loading = ref(false)
  const stId = ref<number>(-1)
  const stockDetails = ref<StockDetail[]>([])
  const initStockDetail = ref<StockDetail>({
    id: -1,
    QOH: 0,
    balance: 0,
    materialId: -1,
    StockId: -1
  })
  const editedStockDetail = ref<StockDetail>(JSON.parse(JSON.stringify(initStockDetail.value)))

  //Read
  async function getStockDetails() {
    loadingStore.doLoad()
    const res = await stockDetailService.getStockDetails()
    stockDetails.value = res.data
    loadingStore.finish()
  }

  //Read
  async function getStockDetailsByStockId(stockId: number) {
    loadingStore.doLoad()
    stId.value = stockId
    const res = await stockDetailService.getStockDetailsByStockId(stockId)
    const stockDetialsData = res.data.map((stockDetail: any) => {
      return {
        id: stockDetail.SD_ID,
        QOH: stockDetail.SD_QOH,
        balance: stockDetail.SD_BALANCE,
        materialId: stockDetail.SD_MATERIAL.M_ID
      }
    })
    stockDetails.value = stockDetialsData
    loadingStore.finish()
  }

  //Create or Update
  async function saveStock(stockDetail: StockDetail) {
    loadingStore.doLoad()
    if (stockDetail.id < 0) {
      //Add new
      const stockDetailForSend = ref<StockDetailForSend>({
        QOH: stockDetail.QOH,
        balance: stockDetail.balance,
        materialId: stockDetail.materialId,
        StockId: stockDetail.StockId
      })
      const res = await stockDetailService.addStockDetail(stockDetailForSend.value)
    } else {
      //Update
      const res = await stockDetailService.updateStockDetail(stockDetail)
    }
    await getStockDetailsByStockId(stId.value)
    loadingStore.finish()
  }

  //for edit dialog
  function editStockDetail(stockDetail: StockDetail) {
    editedStockDetail.value = Object.assign({}, stockDetail)
    dialog.value = true
  }

  //for delete dialog
  function sendToDelete(stockDetail: StockDetail) {
    editedStockDetail.value = stockDetail
    dialogDelete.value = true
  }

  async function deleteItemConfirm(stockDetail: StockDetail) {
    await deleteStock(stockDetail)
    closeDelete()
  }

  async function deleteStock(stockDetail: StockDetail) {
    loadingStore.doLoad()
    const res = await stockDetailService.delStockDetail(stockDetail)
    await getStockDetailsByStockId(stId.value)
    stId.value = -1
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedStockDetail.value = initStockDetail.value
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedStockDetail.value = Object.assign({}, initStockDetail.value)
    })
  }
  function getMaterialName(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.name : 'Unknown Material'
  }
  function getMaterialType(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.type : 'Unknown Material'
  }

  function getMinimum(materialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === materialId)
    return material ? material.minimum : null
  }

  return {
    dialogDelete,
    dialog,
    loading,
    stockDetails,
    initStockDetail,
    editedStockDetail,
    getStockDetails,
    getStockDetailsByStockId,
    saveStock,
    editStockDetail,
    sendToDelete,
    deleteItemConfirm,
    closeDelete,
    closeDialog,
    getMaterialName,
    getMaterialType,
    getMinimum
  }
})
