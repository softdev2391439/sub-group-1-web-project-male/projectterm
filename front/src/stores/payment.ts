import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/products'
export const usePaymentStore = defineStore('payment', () => {
    const loadingStore = useLoadingStore()
    const valpayDialog = ref(false)



    function usevalpayDialog() {
        valpayDialog.value = true
    }

    function closevalpayDialog() {
        valpayDialog.value = false
    }
    return { valpayDialog, usevalpayDialog, closevalpayDialog }
})
