import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import customerService from '@/services/customer'
import type { Customer } from '@/types/Customer'
import customer from '@/services/customer'

export const useCustomerStore = defineStore('customer', () => {
  const loadingStore = useLoadingStore()
  const loading = ref(false)
  const currentCustomer = ref<Customer | null>()
  const dialog = ref(false)
  const form = ref(false)
  const initiCustomer: Customer = {
    name: '',
    tel: '',
    gender: 'female',
    inDate: new Date(),
    pointAmount: 0,
    pointRate: 0
  }
  const customers = ref<Customer[]>([])
  const editedCustomer = ref<Customer>(JSON.parse(JSON.stringify(initiCustomer)))

  function closeDialog() {
    dialog.value = false
  }

  async function getCustomer(id: number) {
    loadingStore.doLoad()
    const res = await customerService.getCustomer(id)
    editedCustomer.value = res.data
    loadingStore.finish()
  }

  async function getCustomers() {
    loadingStore.doLoad()
    const res = await customerService.getCustomers()
    customers.value = res.data
    loadingStore.finish()
  }

  async function saveCustomer(customer: Customer) {
    loadingStore.doLoad()
    if (!customer.id) {
      // Add new
      const res = await customerService.addCustomer(customer)
    } else {
      // Update
      const res = await customerService.updateCustomer(customer)
    }
    await getCustomers()
    loadingStore.finish()
  }

  async function saveCustomer2() {
    try {
      loadingStore.doLoad()
      const customer = editedCustomer.value
      if (!customer.id) {
        // Add new
        console.log('Post ' + JSON.stringify(customer))
        const res = await customerService.addCustomer(customer)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(customer))
        const res = await customerService.updateCustomer(customer)
      }
      await getCustomers()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }

  async function searchMember(tel: string) {
    loadingStore.doLoad()
    const res = await customerService.getCustomerByTel(tel)
    await getCustomers()
    loadingStore.finish()
    return
  }
  async function deleteCustomer(customer: Customer) {
    loadingStore.doLoad()
    const res = await customerService.delCustomer(customer)
    await getCustomers()
    loadingStore.finish()
  }

  const getCustomerByTel = (tel: string): Customer | null => {
    for (const customer of customers.value) {
      if (customer.tel === tel) return customer
    }
    return null
  }

  function onSubmit() {}
  function clear() {
    currentCustomer.value = null
  }

  return {
    clear,
    saveCustomer2,
    onSubmit,
    form,
    loading,
    customers,
    getCustomer,
    getCustomers,
    searchMember,
    saveCustomer,
    deleteCustomer,
    getCustomerByTel,
    closeDialog,
    editedCustomer,
    currentCustomer,
    dialog
  }
})
