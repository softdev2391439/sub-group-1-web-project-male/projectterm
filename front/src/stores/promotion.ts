import { ref, computed, onMounted } from 'vue'
import { defineStore } from 'pinia'
import promotionService from '@/services/promotion'
import type { Promotion } from '@/types/Promotion'
import { useLoadingStore } from './loading'

export const usePromotionStore = defineStore('promotion', () => {
  const promotionDialog = ref(false)
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])
  const headers = [
    {
      title: 'id',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Discount',
      key: 'discount',
      value: 'discount'
    },
    {
      title: 'PointUse',
      key: 'pointUse',
      value: 'pointUse'
    },

    { title: 'Select', key: 'actions', sortable: false }
  ]
  async function getSinglePromotion(id: number) {
    const res = await promotionService.getPromotion(id)
    promotions.value = res.data
  }

  function showPromotion() {
    promotionDialog.value = true
  }
  async function getPromotion() {
    loadingStore.doLoad()
    const res = await promotionService.getAllPromotion()
    promotions.value = res.data
    loadingStore.finish()
  }
  async function savePromotion(promotions: Promotion) {
    loadingStore.doLoad()
    if (promotions.id < 0) {
      console.log('Post' + JSON.stringify(promotions))
      const res = await promotionService.addPromotion(promotions)
    } else {
      console.log('patch' + JSON.stringify(promotions))
      const res = await promotionService.updatePromotion(promotions)
    }
    getPromotion()
    loadingStore.finish()
  }
  async function deletePromotion(promotions: Promotion) {
    loadingStore.doLoad()
    const res = await promotionService.delPromotion(promotions)
    getPromotion()
    loadingStore.finish()
  }
  onMounted(() => {
    getPromotion()
  })
  return {
    promotions,
    promotionDialog,
    headers,
    getPromotion,
    deletePromotion,
    savePromotion,
    getSinglePromotion,
    showPromotion
  }
})
