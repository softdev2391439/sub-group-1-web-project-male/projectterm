import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const currentUser = ref<User | any>()
  const users = ref<User[]>([])
  const initialUser: User & { files: File[] } = {
    name: 'นพาพร บุญพรรณ',
    gender: '',
    height: 0,
    weight: 0,
    bloodType: '',
    age: 0,
    birthDate: new Date(),
    phone: '084-123-7890',
    email: 'noppa@dc.th',
    address: '180 หมู่ 2 ต.บางแสน อ.บางแสน จ.ชลบุรี 20131',
    role: {
      id: 1,
      name: 'เจ้าของร้าน'
    },
    startDate: new Date(),
    status: 'ยังอยู่',
    salary: 250000,
    branch: {
      id: 3,
      name: 'คณะวิทยาการสาระสนเทศ มหาวิทยาลัยบูรพา',
      address: 'ไม่รู้อะ',
      city: 'บางแสน',
      province: 'ชลบุรี',
      country: 'ไทย',
      postalcode: '20131',
      latitude: 13.281264959275443,
      longitude: 100.92410270978652
    },
    password: 'Pass@1234',
    image: 'noimage.jpg',
    files: []
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))
  async function getUser(id: number) {
    loadingStore.doLoad()
    try {
      const res = await userService.getUser(id)
      editedUser.value = res.data
    } catch (error) {
      console.error('Error fetching user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function getUsers() {
    loadingStore.doLoad()
    try {
      const res = await userService.getUsers()
      users.value = res.data
    } catch (error) {
      console.error('Error fetching users:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function saveUser() {
    loadingStore.doLoad()
    try {
      const user = editedUser.value
      let res
      if (!user.id) {
        console.log('Post ' + JSON.stringify(user))
        res = await userService.addUser(user)
      } else {
        console.log('Patch ' + JSON.stringify(user))
        res = await userService.updateUser(user)
      }
      if (res) {
        await getUsers()
      }
    } catch (error) {
      console.error('Error saving user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function deleteUser() {
    loadingStore.doLoad()
    try {
      const user = editedUser.value
      await userService.delUser(user)
      await getUsers()
    } catch (error) {
      console.error('Error deleting user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }

  async function changePassword() {
    const user = editedUser.value
    await userService.changePassword(user)
    clearForm()
  }

  return {
    users,
    getUsers,
    saveUser,
    deleteUser,
    editedUser,
    getUser,
    clearForm,
    currentUser,
    changePassword
  }
})
