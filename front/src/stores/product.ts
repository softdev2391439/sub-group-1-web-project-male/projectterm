import { ref, nextTick, type Ref } from 'vue'
import { defineStore } from 'pinia'

import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/products'

export const useProductStore = defineStore('product', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loadingStore = useLoadingStore()
  let editedIndex = -1

  const initiProduct: Product & { files: File[] } = {
    name: '',
    image: 'noimage.PNG',
    price: '',
    unit: '',
    category: { id: 1, name: 'Drink' },
    subCategorys: [],
    sizes: [],
    files: []
  }

  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initiProduct)))
  const products = ref<Product[]>([])

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedProduct.value = Object.assign({}, initiProduct)
    })
  }

  function checkDrink() {
    if (editedProduct.value.category.name === 'Drink') {
      return true
    }
    editedProduct.value.subCategorys = []
    editedProduct.value.sizes = []
    return false
  }

  async function deleteItem(item: Product) {
    if (!item.id) return
    await getProduct(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  async function getProducts() {
    loadingStore.doLoad()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }
  async function getProduct(id: number) {
    loadingStore.doLoad()
    const res = await productService.getProduct(id)
    editedProduct.value = res.data
    loadingStore.finish()
  }

  async function deleteItemConfirm() {
    await deleteProduct()
    closeDelete()
  }

  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
    loadingStore.finish()
  }

  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }
      await getProducts()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initiProduct))
  }

  async function editItem(item: Product) {
    if (!item.id) return
    await getProduct(item.id)
    dialog.value = true
  }
  return {
    products,
    dialog,
    dialogDelete,
    editedProduct,
    initiProduct,
    closeDelete,
    saveProduct,
    deleteItemConfirm,
    getProducts,
    clearForm,
    getProduct,
    deleteItem,
    checkDrink,
    editItem
  }
})
