import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMapStore = defineStore('map', () => {
  const lat = ref(0)
  const lng = ref(0)
  const branchAddress = ref<{ lat: number; lng: number; name: string } | null>(null)
  const map = ref()
  const popup = ref()
  const currentLat = ref(0)
  const currentLng = ref(0)
  const currentMarker = ref(null)
  const currentLayer = ref(null)
  const flageMarker = ref(null)
  const brandMarker = ref(null)
  return {
    lat,
    lng,
    branchAddress,
    map,
    popup,
    currentMarker,
    currentLayer,
    flageMarker,
    brandMarker,
    currentLat,
    currentLng
  }
})
