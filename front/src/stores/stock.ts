import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'

//import store
import { useMaterialStore } from './material'
import { useUserStore } from './user'
//import type
import type { Stock } from '@/types/Stock'
import type { StockDetail } from '@/types/StockDetail'

import type { Material } from '@/types/Material'

import http from '@/services/http'
import stocksService from '@/services/stock'
import { useLoadingStore } from '@/stores/loading'

import stockService from '@/services/stock'
import { useBranchStore } from './branch'
import material from '@/services/material'

export const useStockStore = defineStore('stockStore', () => {
  //set name of store
  const userStore = useUserStore()
  const materialStore = useMaterialStore()

  const loadingStore = useLoadingStore()
  const branchStore = useBranchStore()

  //set variable Dialog
  const addAllDialog = ref(false)
  const dialogDelete = ref(false)
  const editItemDialog = ref(false)
  const addNewMaterialDialog = ref(false)
  const StockDialog = ref(false)
  const saveMaterialDialog = ref(false)
  const addNewStockItemDialog = ref(false)
  const addSomeDialog = ref(false)
  const amount = ref(0)
  const loading = ref(false)

  const initStock = ref<Stock>({
    id: -1,
    date: '',
    userID: -1,
    branchID: -1,
    stockDetails: []
  })

  const stocks = ref<Stock[]>([])

  const editedStock = ref<Stock>(initStock.value)

  const initStockItem: StockDetail = {
    id: -1,
    QOH: 0,
    balance: 0,
    materialId: -1,
    StockId: -1
  }

  const stockItems = ref<StockDetail[]>([])
  const editStockItem = ref<StockDetail>(JSON.parse(JSON.stringify(initStockItem)))

  //for get value for ui to set new material
  const name = ref('')
  const minimum = ref(0)
  const typ = ref('')

  function initializeItem() {
    stockItems.value = []
  }

  function getUserName(userID: number) {
    const user = userStore.users.find((user) => user && user.id === userID)
    return user ? user.name : 'Unknown User'
  }

  function getBranchName(branchID: number) {
    const branch = branchStore.branches.find((branch) => branch && branch.id === branchID)
    return branch ? branch.name : 'Unknown Branch'
  }

  function formatDate(date: Date) {
    const year = date.getFullYear().toString().padStart(4, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const day = date.getDate().toString().padStart(2, '0')
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')

    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
    return formattedDate
  }
  function close() {
    stockItems.value = []
    addNewStockItemDialog.value = false
    addSomeDialog.value = false
    addAllDialog.value = false
  }

  async function getStocks() {
    loadingStore.doLoad()
    try {
      const res = await stockService.getStocks()
      const stocksData = res.data.map((stock: any) => {
        let userID = -1

        if (stock.user && stock.user.id) {
          userID = stock.user.id
        }
        return {
          id: stock.S_ID,
          date: formatDate(new Date(stock.created)),
          userID: stock.S_USER.id,
          branchID: stock.S_BRANCH.id,
          stockDetails: stock.S_STOCKDETAILS
        }
      })
      stocks.value = stocksData
      loadingStore.finish()
    } catch (e) {
      console.log('cant fetch data from database')
      loadingStore.finish()
    }
  }

  function sendToDelete(stock: Stock) {
    editedStock.value = stock
    dialogDelete.value = true
  }

  async function deleteItemConfirm(stock: Stock) {
    await deleteStock(stock)
    closeDelete()
  }

  async function deleteStock(stock: Stock) {
    loadingStore.doLoad()
    const res = await stocksService.delStock(stock)
    await getStocks()
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedStock.value = initStock.value
  }

  function editStock(item: Stock) {
    editedStock.value = Object.assign({}, item)
    addAllDialog.value = true
  }

  async function saveStock(stock: Stock) {
    loadingStore.doLoad()
    if (stock.id < 0) {
      //Add new
      const res = await stocksService.addStock(stock)
    } else {
      //Update
      const res = await stocksService.updateStock(stock)
    }
    getStocks()
    loadingStore.finish()
  }

  function closeDialog() {
    addAllDialog.value = false
    addSomeDialog.value = false
    nextTick(() => {
      editedStock.value = Object.assign({}, initStock.value)
      materialStore.addsomeMaterial = []
    })
  }

  return {
    stockItems,
    addAllDialog,
    dialogDelete,
    editStockItem,
    deleteItemConfirm,
    editStock,
    deleteStock,
    closeDelete,
    initStockItem,
    initializeItem,
    editItemDialog,
    //updateQOHItems,
    typ,
    minimum,
    name,
    addNewMaterialDialog,
    saveMaterialDialog,
    stocks,
    getUserName,
    getBranchName,
    formatDate,
    StockDialog,
    addNewStockItemDialog,
    addSomeDialog,
    amount,
    close,
    editedStock,
    getStocks,
    saveStock,
    closeDialog,
    loading,
    sendToDelete
  }
})
