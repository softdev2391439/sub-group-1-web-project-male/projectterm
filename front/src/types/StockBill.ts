import type { StockBillDetail } from './StockBillDetail'
type StockBill = {
  id: number
  creatDate: Date
  total: number
  totalItem: number
  userID: number
  paid: number
  stockBillDeatils: StockBillDetail[]
}

export type { StockBill }
