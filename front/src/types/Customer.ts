type Gender = 'male' | 'female' | 'other'

type Customer = {
  id?: number
  name: string
  tel: string
  gender: Gender
  inDate: Date
  pointAmount: number
  pointRate: number
}
export type { Customer }
