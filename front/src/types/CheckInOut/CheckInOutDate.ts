type CheckUser = {
  id: number
  name: string
  timeIn: string
  timeOut: string
  totalMin: number
}

type CheckDate = {
  id: number
  date: Date
  checkUser: CheckUser[]
}

export type { CheckDate, CheckUser }
