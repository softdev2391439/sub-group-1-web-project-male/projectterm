import type { Product } from './Product'

type SaveReceiptItem = {
  id?: number
  name: string
  price: string
  unit: string
  productsId?: string
  products?: Product
}

export { type SaveReceiptItem }
