import type { Customer } from './Customer'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type SaveReceipt = {
    id?: number
    createdDate?: Date
    totalBefore: string
    customerDiscount: string
    total: string
    receivedAmount: string
    change: string
    PaymentType: string
    userId?: number
    user?: User
    memberId?: number
    member?: Customer
    receiptItems?: ReceiptItem[]
    PromotionDiscount: string
    PointDiscount: string
    AllDiscount: string
}

export type { SaveReceipt }