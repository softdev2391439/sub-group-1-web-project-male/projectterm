import type { Customer } from './Customer'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id?: number
  createdDate?: Date
  totalBefore: number
  customerDiscount: number
  total: number
  receivedAmount: number
  change: number
  PaymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Customer
  receiptItems?: ReceiptItem[]
  PromotionDiscount: number
  PointDiscount: number
  AllDiscount: number
}

export type { Receipt }
