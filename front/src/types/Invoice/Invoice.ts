import type { InvoiceDetail } from './InvoiceDetail'

type Invoice = {
  INV_ID: number
  INV_PLACE: string
  INV_PRICE: number
  INV_BRANCH: number
  INV_USER: number
  INV_INVOICEDETAILS: InvoiceDetail[]
}

export type { Invoice }
