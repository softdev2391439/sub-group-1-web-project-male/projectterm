import type { Category } from './Category'
import type { Size } from './Size'
import type { SubCategory } from './SubCategory'

type SweetLevel = '0' | '25' | '50' | '100'

type Product = {
  id?: number
  name: string
  price: string
  image: string
  unit: string
  category: Category
  subCategorys?: SubCategory[]
  sweetLevel?: SweetLevel[]
  sizes?: Size[]
  size?: Size
  subCategory?: SubCategory
}

export type { Product }
