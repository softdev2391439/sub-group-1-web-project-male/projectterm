import type { User } from '../User'
import type { UtilityDetail } from './UtilityDetail'

type UtilityCost = {
  id?: number
  creatDate: Date
  totalPrice: number
  totalItem: number
  user: User
  ucDetails: UtilityDetail[]
}

export type { UtilityCost }
