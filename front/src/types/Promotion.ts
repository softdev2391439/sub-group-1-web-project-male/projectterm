type Status = 'Active' | 'Inactive'

type Promotion = {
  id: number
  name: string
  startDate: Date
  endDate: Date
  details: string
  discount: number
  pointUse: number
  status: Status
}

export type { Promotion }
