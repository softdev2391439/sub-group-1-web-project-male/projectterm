import type { Customer } from '@/types/Customer'
import http from './http'

function addCustomer(customer: Customer) {
  return http.post('/customers', customer)
}

function updateCustomer(customer: Customer) {
  return http.patch(`/customers/${customer.id}`, customer)
}

function delCustomer(customer: Customer) {
  return http.delete(`/customers/${customer.id}`)
}

function getCustomerByTel(tel: string) {
  return http.get(`/customers/tel/${tel}`)
}

function getCustomer(id: number) {
  return http.get(`/customers/${id}`)
}

function getCustomers() {
  return http.get('/customers')
}

export default {
  getCustomerByTel,
  addCustomer,
  updateCustomer,
  delCustomer,
  getCustomer,
  getCustomers
}
