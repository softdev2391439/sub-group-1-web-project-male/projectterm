import type { StockDetail } from '../types/StockDetail'
import type { StockDetailForSend } from '@/types/StockDetailAddnew'
import http from './http'

function addStockDetail(stockDetail: StockDetailForSend) {
  return http.post('/stockDetails', stockDetail)
}
function updateStockDetail(stockDetail: StockDetail) {
  return http.patch(`/stockDetails/${stockDetail.id}`, stockDetail)
}
function delStockDetail(stockDetail: StockDetail) {
  return http.delete(`/stockDetails/${stockDetail.id}`)
}
function getStockDetail(id: number) {
  return http.get(`/stockDetails/${id}`)
}
function getStockDetails() {
  return http.get('/stockDetails')
}
function getStockDetailsByStockId(stockId: number) {
  return http.get(`/stockDetails/stockId/${stockId}`)
}

export default {
  addStockDetail,
  updateStockDetail,
  delStockDetail,
  getStockDetail,
  getStockDetails,
  getStockDetailsByStockId
}
