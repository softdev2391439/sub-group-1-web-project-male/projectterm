import type { Size } from '@/types/Size'
import http from './http'

function addSize(size: Size) {
  return http.post('/sizes', size)
}
function updateSize(size: Size) {
  return http.patch(`/sizes/${size.id}`, size)
}
function delSize(size: Size) {
  return http.delete(`/sizes/${size.id}`)
}
function getSize(id: number) {
  return http.get(`/sizes/${id}`)
}
function getSizes() {
  return http.get('/sizes')
}

export default { addSize, updateSize, delSize, getSize, getSizes }
