import type { Product } from '@/types/Product'
import http from './http'
import type { Receipt } from '@/types/Receipt'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { SaveReceipt } from '@/types/SaveReceipt'

function addReceipt(saveReceipt: SaveReceipt, receiptItem: ReceiptItem[]) {
  const formData = new FormData()
  formData.append('total', saveReceipt.total)
  formData.append('receivedAmount', saveReceipt.receivedAmount)
  formData.append('change', saveReceipt.change)
  formData.append('PaymentType', saveReceipt.PaymentType)
  formData.append('totalBefore', saveReceipt.totalBefore)
  formData.append('customerDiscount', saveReceipt.customerDiscount)
  formData.append('PromotionDiscount', saveReceipt.PromotionDiscount)
  formData.append('AllDiscount', saveReceipt.AllDiscount)
  formData.append('user', JSON.stringify(saveReceipt.user))
  formData.append('customer', JSON.stringify(saveReceipt.member))
  formData.append('receiptitems', JSON.stringify(receiptItem))
  return http.post('/receipts', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price)
  formData.append('unit', product.unit)
  formData.append('category', JSON.stringify(product.category))
  if (product.subCategorys && product.subCategorys.length > 0) {
    formData.append('subCategorys', JSON.stringify(product.subCategorys))
  } else {
    formData.append('subCategorys', JSON.stringify(product.subCategorys))
  }
  if (product.sizes && product.sizes.length > 0) {
    formData.append('sizes', JSON.stringify(product.sizes))
  } else {
    formData.append('sizes', JSON.stringify(product.sizes))
  }
  if (product.files && product.files.length > 0) {
    formData.append('file', product.files[0])
  }
  return http.post(`/products/${product.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function getProductsByType(typeId: number) {
  return http.get(`/products/types/${typeId}`)
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}
function getProduct(id: number) {
  return http.get(`/products/${id}`)
}
function getProducts() {
  return http.get('/products')
}

export default { addReceipt, updateProduct, delProduct, getProduct, getProducts, getProductsByType }
