import http from './http'
import type { SubCategory } from '@/types/SubCategory'

function addSubCategory(subCategory: SubCategory) {
  return http.post('/sub-categorys', subCategory)
}
function updateSubCategory(subCategory: SubCategory) {
  return http.patch(`/sub-categorys/${subCategory.id}`, subCategory)
}
function delSubCategory(subCategory: SubCategory) {
  return http.delete(`/sub-categorys/${subCategory.id}`)
}
function getSubCategory(id: number) {
  return http.get(`/sub-categorys/${id}`)
}
function getSubCategorys() {
  return http.get('/sub-categorys')
}

export default {
  addSubCategory,
  updateSubCategory,
  delSubCategory,
  getSubCategory,
  getSubCategorys
}
