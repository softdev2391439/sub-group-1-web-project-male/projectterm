import type { UtilityCost } from '@/types/utiliycost/UtilityCost'
import http from './http'

function addUtilityCost(utilityCost: UtilityCost) {
  return http.post('/utility-cost', utilityCost)
}
function delUtilityCost(utilityCost: UtilityCost) {
  return http.delete(`/utility-cost/${utilityCost.id}`)
}
function getUtilityCost(id: number) {
  return http.get(`/utility-cost/${id}`)
}
function getUtilityCostes() {
  console.log('getUtilityCostes')
  return http.get('/utility-cost')
}
export default { addUtilityCost, delUtilityCost, getUtilityCost, getUtilityCostes }
