import type { Branch } from '@/types/Branch'
import http from './http'

function addBranch(branch: Branch) {
  return http.post('/branches', branch)
}
function updateBranch(branch: Branch) {
  return http.patch(`/branches/${branch.id}`, branch)
}
function delBranch(branch: Branch) {
  return http.delete(`/branches/${branch.id}`)
}
function getBranch(id: number) {
  return http.get(`/branches/${id}`)
}
function getBranches() {
  console.log('getBranches')
  return http.get('/branches')
}
export default { addBranch, updateBranch, delBranch, getBranch, getBranches }
