import type { Promotion } from '@/types/Promotion'

import http from './http'
function addPromotion(promotions: Promotion) {
  const { id, ...promotionWithoutId } = promotions
  return http.post('/promotion', promotionWithoutId)
}

function updatePromotion(promotions: Promotion) {
  return http.patch(`/promotion/${promotions.id} `, promotions)
}

function delPromotion(promotions: Promotion) {
  return http.delete(`/promotion/${promotions.id} `)
}

function getPromotion(id: number) {
  return http.get(`/promotion/${id} `)
}

function getAllPromotion() {
  return http.get('/promotion')
}

export default { addPromotion, delPromotion, getAllPromotion, getPromotion, updatePromotion }
