import type { InvoiceDetail } from '@/types/Invoice/InvoiceDetail'
import http from './http'

function addInvoiceDetail(invoiceDetail: InvoiceDetail) {
  const { IND_ID, ...invoiceWithOutId } = invoiceDetail
  return http.post('/invoiceDetails', invoiceWithOutId)
}
function updateInvoiceDetail(invoiceDetail: InvoiceDetail) {
  const { IND_ID, ...invoiceWithOutId } = invoiceDetail
  return http.patch(`/invoiceDetails/${invoiceDetail.IND_ID}`, invoiceWithOutId)
}
function delInvoiceDetail(invoiceDetail: InvoiceDetail) {
  return http.delete(`/invoiceDetails/${invoiceDetail.IND_ID}`)
}
function getInvoiceDetail(id: number) {
  return http.get(`/invoiceDetails/${id}`)
}
function getInvoiceDetails() {
  return http.get('/invoiceDetails')
}
function getInvoiceDetailsByInvoiceId(invoiceId: number) {
  return http.get(`/stockDetails/invoiceId/${invoiceId}`)
}

export default {
  addInvoiceDetail,
  updateInvoiceDetail,
  delInvoiceDetail,
  getInvoiceDetail,
  getInvoiceDetails,
  getInvoiceDetailsByInvoiceId
}
