import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/material',
      name: 'materail',
      component: () => import('../views/Material/MaterialTable.vue')
    },
    {
      path: '/stock',
      name: 'stock',
      component: () => import('../views/Stock/StockView.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/pos-view',
      name: 'pos',
      component: () => import('../views/POS/POSView.vue')
    },
    {
      path: '/check',
      name: 'check',
      component: () => import('../views/CheckInOut.vue')
    },
    {
      path: '/invoice',
      name: 'invoice',
      component: () => import('../views/Material/InvoiceDialogView.vue')
    },
    {
      path: '/checkLogin',
      name: 'checkLogin',
      component: () => import('../components/CheckInOut/CheckInOutLog.vue')
    },
    {
      path: '/checkTable',
      name: 'checkTable',
      component: () => import('../components/CheckInOut/CheckInOutTable.vue')
    },
    {
      path: '/addsome',
      name: 'addsome',
      component: () => import('../views/Material/AddSomeMaterialDialog.vue')
    },
    {
      path: '/newulic',
      name: 'newulitycost',
      component: () => import('../views/Utilitycost Pay/UtilityNewBill.vue')
    },
    {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/SalaryView.vue')
    },
    {
      path: '/ulic',
      name: 'utilitycost',
      component: () => import('../views/Utilitycost Pay/UtilityCost.vue')
    },
    {
      path: '/img',
      name: 'image',
      component: () => import('../components/UploadUserImage.vue')
    },
    {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/CustomerCRUDView.vue')
    },
    {
      path: '/temperature',
      name: 'Temperature',
      component: () => import('../views/TemperatureView.vue')
    },
    {
      path: '/role',
      name: 'role',
      component: () => import('../views/Management/RoleTableView.vue')
    },
    {
      path: '/branch',
      name: 'branch',
      component: () => import('../views/Management/BranchTableView.vue')
    },
    {
      path: '/map',
      name: 'map',
      component: () => import('../views/Management/MapView.vue')
    },
    {
      path: '/pos',
      name: 'pos',
      component: () => import('../views/POS/POSView.vue')
    },
    {
      path: '/userCRUD',
      name: 'userCRUD',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Management/UserCRUD.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        requireAuthentication: true
      }
    },
    {
      path: '/profile',
      name: 'profile',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Management/ProfileView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        requireAuthentication: true
      }
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('../views/ProductView.vue')
    },
    {
      path: '/promotion',
      name: 'promotion',
      component: () => import('../views/PromotionView.vue')
    },
    {
      path: '/mainstock',
      name: 'mainstock',
      component: () => import('@/views/Stock/MainStock.vue')
    }
  ]
})

const isLogin = () => {
  const user = localStorage.getItem('user')
  if (user === null) {
    return false
  }
  return true
}

router.beforeEach((to, from) => {
  if (to.meta.requireAuthentication && !isLogin()) {
    return {
      path: '/login',
      query: { redirect: to.fullPath }
    }
    //or
    //router.replace('/login')
  }
})

export default router
