import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptitemService } from './receiptitem.service';

describe('ReceiptitemService', () => {
  let service: ReceiptitemService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReceiptitemService],
    }).compile();

    service = module.get<ReceiptitemService>(ReceiptitemService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
