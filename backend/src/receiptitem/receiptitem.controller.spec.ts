import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptitemController } from './receiptitem.controller';
import { ReceiptitemService } from './receiptitem.service';

describe('ReceiptitemController', () => {
  let controller: ReceiptitemController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReceiptitemController],
      providers: [ReceiptitemService],
    }).compile();

    controller = module.get<ReceiptitemController>(ReceiptitemController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
