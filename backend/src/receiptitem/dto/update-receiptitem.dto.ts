import { PartialType } from '@nestjs/swagger';
import { CreateReceiptitemDto } from './create-receiptitem.dto';

export class UpdateReceiptitemDto extends PartialType(CreateReceiptitemDto) {}
