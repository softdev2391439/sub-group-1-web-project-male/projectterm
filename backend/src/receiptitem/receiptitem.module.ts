import { Module } from '@nestjs/common';
import { ReceiptitemService } from './receiptitem.service';
import { ReceiptitemController } from './receiptitem.controller';

@Module({
  controllers: [ReceiptitemController],
  providers: [ReceiptitemService],
})
export class ReceiptitemModule {}
