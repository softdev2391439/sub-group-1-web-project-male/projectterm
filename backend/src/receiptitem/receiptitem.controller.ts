import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ReceiptitemService } from './receiptitem.service';
import { CreateReceiptitemDto } from './dto/create-receiptitem.dto';
import { UpdateReceiptitemDto } from './dto/update-receiptitem.dto';

@Controller('receiptitem')
export class ReceiptitemController {
  constructor(private readonly receiptitemService: ReceiptitemService) {}

  @Post()
  create(@Body() createReceiptitemDto: CreateReceiptitemDto) {
    return this.receiptitemService.create(createReceiptitemDto);
  }

  @Get()
  findAll() {
    return this.receiptitemService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.receiptitemService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateReceiptitemDto: UpdateReceiptitemDto) {
    return this.receiptitemService.update(+id, updateReceiptitemDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptitemService.remove(+id);
  }
}
