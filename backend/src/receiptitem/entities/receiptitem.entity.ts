import { Category } from 'src/category/entities/category.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Size } from 'src/sizes/entities/size.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { User } from 'src/users/entities/user.entity';
import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';


@Entity()
export class Receiptitem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    price: number

    @Column()
    unit: number

    @ManyToOne(() => Product, (product) => product.receiptitems)
    product: Product;

    @ManyToMany(() => Receipt, (receipt) => receipt.receiptitems)
    receipts: Receipt[];
}
