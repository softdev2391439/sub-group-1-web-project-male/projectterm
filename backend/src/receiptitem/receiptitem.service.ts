import { Injectable } from '@nestjs/common';
import { CreateReceiptitemDto } from './dto/create-receiptitem.dto';
import { UpdateReceiptitemDto } from './dto/update-receiptitem.dto';

@Injectable()
export class ReceiptitemService {
  create(createReceiptitemDto: CreateReceiptitemDto) {
    return 'This action adds a new receiptitem';
  }

  findAll() {
    return `This action returns all receiptitem`;
  }

  findOne(id: number) {
    return `This action returns a #${id} receiptitem`;
  }

  update(id: number, updateReceiptitemDto: UpdateReceiptitemDto) {
    return `This action updates a #${id} receiptitem`;
  }

  remove(id: number) {
    return `This action removes a #${id} receiptitem`;
  }
}
