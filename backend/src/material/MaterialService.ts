import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.M_NAME = createMaterialDto.name;
    material.M_AMOUNT = createMaterialDto.amount;
    material.M_MIN = createMaterialDto.minimum;
    material.M_TYPE = createMaterialDto.type;
    return await this.materialsRepository.save(material);
  }

  async findAll() {
    return await this.materialsRepository.find();
  }

  async findOne(id: number) {
    return await this.materialsRepository.findOneOrFail({
      where: { M_ID: id },
    });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneOrFail({
      where: { M_ID: id },
    });
    material.M_NAME = updateMaterialDto.name;
    material.M_AMOUNT = updateMaterialDto.amount;
    material.M_MIN = updateMaterialDto.minimum;
    material.M_TYPE = updateMaterialDto.type;
    await this.materialsRepository.update(id, material);
    return await this.materialsRepository.findOneOrFail({
      where: { M_ID: id },
    });
  }

  async remove(id: number) {
    const delMaterial = await this.materialsRepository.findOneOrFail({
      where: { M_ID: id },
    });
    await this.materialsRepository.delete(delMaterial);
    return delMaterial;
  }
}
