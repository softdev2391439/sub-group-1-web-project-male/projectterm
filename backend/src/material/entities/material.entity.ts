import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  M_ID: number;

  @Column()
  M_NAME: string;

  @Column()
  M_MIN: number;

  @Column()
  M_AMOUNT: number;

  @Column()
  M_TYPE: string;
}
