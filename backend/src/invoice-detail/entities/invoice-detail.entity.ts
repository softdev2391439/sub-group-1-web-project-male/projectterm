import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Material } from 'src/material/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  Double,
  Entity,
  JoinTable,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class InvoiceDetail {
  @PrimaryGeneratedColumn()
  IND_ID: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Invoice, (Invoice) => Invoice.INV_INVOICEDETAILS)
  IND_INVOICE: Invoice;

  @ManyToOne(() => Material)
  @JoinTable({ name: 'M_ID' })
  IND_MATERIAL: Material;

  @Column({ type: 'real' })
  IND_AMOUT: Double;

  @Column({ type: 'real' })
  IND_PRICE: Double;
}
