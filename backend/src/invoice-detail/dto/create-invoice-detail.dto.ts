import { Double } from 'typeorm';

export class CreateInvoiceDetailDto {
  IND_INVOICE: number;

  IND_MATERIAL: number;

  IND_AMOUT: Double;

  IND_PRICE: Double;
}
