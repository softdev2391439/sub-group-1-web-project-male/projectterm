import { Injectable } from '@nestjs/common';
import { CreateInvoiceDetailDto } from './dto/create-invoice-detail.dto';
import { UpdateInvoiceDetailDto } from './dto/update-invoice-detail.dto';
import { InvoiceDetail } from './entities/invoice-detail.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Material } from 'src/material/entities/material.entity';

@Injectable()
export class InvoiceDetailService {
  constructor(
    @InjectRepository(InvoiceDetail)
    private invoiceDetailsRepository: Repository<InvoiceDetail>,
    @InjectRepository(Invoice)
    private invoicesRepository: Repository<Invoice>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createInvoiceDetailDto: CreateInvoiceDetailDto) {
    const invoiceDetail = new InvoiceDetail();
    invoiceDetail.IND_AMOUT = createInvoiceDetailDto.IND_AMOUT;
    invoiceDetail.IND_PRICE = createInvoiceDetailDto.IND_PRICE;
    if (createInvoiceDetailDto.IND_INVOICE > 0) {
      const invoice = await this.invoicesRepository.findOneOrFail({
        where: { INV_ID: createInvoiceDetailDto.IND_INVOICE },
      });
      invoiceDetail.IND_INVOICE = invoice;
    }
    if (createInvoiceDetailDto.IND_INVOICE > 0) {
      const material = await this.materialsRepository.findOneOrFail({
        where: { M_ID: createInvoiceDetailDto.IND_MATERIAL },
      });
      invoiceDetail.IND_MATERIAL = material;
    }
    return this.invoiceDetailsRepository.save(invoiceDetail);
  }

  findAll() {
    return this.invoiceDetailsRepository.find({
      relations: ['IND_MATERIAL'],
    });
  }

  findOne(id: number) {
    return this.invoiceDetailsRepository.findOneOrFail({
      where: { IND_ID: id },
      relations: ['IND_MATERIAL'],
    });
  }

  findByStockId(invoiceId: number) {
    return this.invoiceDetailsRepository.find({
      where: { IND_INVOICE: { INV_ID: invoiceId } },
      relations: ['IND_MATERIAL'],
    });
  }

  async update(id: number, updateInvoiceDetailDto: UpdateInvoiceDetailDto) {
    await this.invoiceDetailsRepository.findOneByOrFail({ IND_ID: id });
    const invoiceDetail = new InvoiceDetail();
    invoiceDetail.IND_ID = id;
    invoiceDetail.IND_AMOUT = updateInvoiceDetailDto.IND_AMOUT;
    invoiceDetail.IND_PRICE = updateInvoiceDetailDto.IND_PRICE;
    if (
      updateInvoiceDetailDto.IND_MATERIAL &&
      updateInvoiceDetailDto.IND_MATERIAL > 0
    ) {
      const material = await this.materialsRepository.findOneOrFail({
        where: { M_ID: updateInvoiceDetailDto.IND_MATERIAL },
      });
      invoiceDetail.IND_MATERIAL = material;
    }

    await this.invoiceDetailsRepository.update(id, invoiceDetail);
    const updatedInvoiceDetail = await this.invoiceDetailsRepository.findOneBy({
      IND_ID: id,
    });
    return updatedInvoiceDetail;
  }

  async remove(id: number) {
    const deleteInvoiceDetail =
      await this.invoiceDetailsRepository.findOneOrFail({
        where: { IND_ID: id },
      });
    await this.invoiceDetailsRepository.remove(deleteInvoiceDetail);

    return deleteInvoiceDetail;
  }
}
