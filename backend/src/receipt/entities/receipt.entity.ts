import { Category } from 'src/category/entities/category.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Receiptitem } from 'src/receiptitem/entities/receiptitem.entity';
import { Size } from 'src/sizes/entities/size.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { User } from 'src/users/entities/user.entity';
import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Receipt {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    total: number;

    @Column()
    receivedAmount: number;

    @Column()
    change: number;

    @Column()
    PaymentType: string;

    @Column()
    totalBefore: number;

    @Column({
        default: 0,
    })
    customerDiscount: number;

    @Column({
        default: 0,
    })
    PromotionDiscount: number;

    @Column({
        default: 0,
    })
    PointDiscount: number;

    @Column({
        default: 0,
    })
    AllDiscount: number;

    @CreateDateColumn()
    created: Date;

    @UpdateDateColumn()
    updated: Date;

    // @ManyToMany(() => Product, (product) => product.receipts, {
    //     cascade: true,
    // })
    // @JoinTable()
    // products: Product[];

    @ManyToOne(() => User, (user) => user.receipts)
    user: User;

    @ManyToOne(() => Customer, (customer) => customer.receipts)
    customer: Customer;

    @ManyToMany(() => Promotion, (promotion) => promotion.receipts, {
        cascade: true,
    })
    @JoinTable()
    promotions: Promotion[];

    @ManyToMany(() => Receiptitem, (receiptitem) => receiptitem.receipts, {
        cascade: true,
    })
    @JoinTable()
    receiptitems: Receiptitem[];
}
