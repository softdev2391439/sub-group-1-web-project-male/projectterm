import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Stock } from 'src/stocks/entities/stock.entity';
import { stocksModule } from 'src/stocks/stock.module';
import { User } from 'src/users/entities/user.entity';
import { StockDetail } from 'src/stockDetails/entities/stockDetail.entity';
import { UsersModule } from 'src/users/users.module';
import { stockDetailsModule } from 'src/stockDetails/stockDetail.module';
import { AuthModule } from 'src/auth/auth.module';
import { DataSource } from 'typeorm';
import { Branch } from 'src/branches/entities/branch.entity';
import { Role } from 'src/roles/entities/role.entity';
import { BranchesModule } from 'src/branches/branches.module';
import { RolesModule } from 'src/roles/roles.module';
import { MaterialModule } from 'src/material/material.module';
import { Material } from 'src/material/entities/material.entity';
import { ProductsModule } from 'src/products/products.module';
import { Product } from 'src/products/entities/product.entity';
import { Category } from 'src/category/entities/category.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { Size } from 'src/sizes/entities/size.entity';
import { SizesModule } from 'src/sizes/sizes.module';
import { SubCategorysModule } from 'src/sub-categorys/sub-categorys.module';
import { CategoryModule } from 'src/category/category.module';
import { Customer } from 'src/customers/entities/customer.entity';
import { CustomersModule } from 'src/customers/customers.module';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { PromotionModule } from 'src/promotion/promotion.module';
import { UtilityCost } from 'src/utility-cost/entities/utility-cost.entity';
import { UtilityDetail } from 'src/utility-cost/entities/uitlity-datail';
import { UtilityCostModule } from 'src/utility-cost/utility-cost.module';
import { InvoiceModule } from 'src/invoice/invoice.module';
import { InvoiceDetailModule } from 'src/invoice-detail/invoice-detail.module';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { InvoiceDetail } from 'src/invoice-detail/entities/invoice-detail.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { ReceiptModule } from 'src/receipt/receipt.module';
import { ReceiptitemModule } from 'src/receiptitem/receiptitem.module';
import { Receiptitem } from 'src/receiptitem/entities/receiptitem.entity';

@Module({
  imports: [
    //for SQLite
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   entities: [
    //     Stock,
    //     User,
    //     StockDetail,
    //     Role,
    //     Branch,
    //     Material,
    //     Product,
    //     Category,
    //     SubCategory,
    //     Size,
    //     Customer,
    //     Promotion,
    //   ], //entityที่จะใช้
    // }),

    // For Mysql
    // need XAMPP (MySQL)
    // don't have to create User Account in PhpMyAdmin
    // create database name dcoffee in MySQL
    // in first time to run set synchronize = true
    // case have tables then set synchronize = false if not you will get  error

    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'dcoffee1',
      timezone: '+07:00',
      entities: [
        Stock,
        User,
        StockDetail,
        Role,
        Branch,
        Material,
        Product,
        Category,
        SubCategory,
        Size,
        Customer,
        Promotion,
        UtilityCost,
        UtilityDetail,
        Invoice,
        InvoiceDetail,
        Receipt,
        Receiptitem
      ],
      synchronize: false,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '..', 'public'),
    }),
    stocksModule,
    UsersModule,
    stockDetailsModule,
    AuthModule,
    ProductsModule,
    CategoryModule,
    SubCategorysModule,
    RolesModule,
    BranchesModule,
    MaterialModule,
    SizesModule,
    CustomersModule,
    PromotionModule,
    UtilityCostModule,
    InvoiceModule,
    InvoiceDetailModule,
    RolesModule,
    ReceiptModule,
    ReceiptitemModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
