import { Module } from '@nestjs/common';
import { UtilityCostService } from './utility-cost.service';
import { UtilityCostController } from './utility-cost.controller';
import { UtilityCost } from './entities/utility-cost.entity';
import { User } from 'src/users/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UtilityDetail } from './entities/uitlity-datail';

@Module({
  imports: [TypeOrmModule.forFeature([User, UtilityCost, UtilityDetail])],
  controllers: [UtilityCostController],
  providers: [UtilityCostService],
})
export class UtilityCostModule {}
