import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UtilityDetail } from './uitlity-datail';

@Entity('UTILITY_COST')
export class UtilityCost {
  @PrimaryGeneratedColumn({
    name: 'UC_ID',
  })
  id: number;

  @Column({
    name: 'UC_TOTAL_PRICE',
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  totalPrice: number;

  @Column({
    name: 'UC_TOTAL_ITEM',
  })
  totalItem: number;

  @ManyToOne(() => User, (user) => user.utilityCosts)
  @JoinColumn({
    name: 'EMP_ID',
  })
  user: User;
  @OneToMany(
    () => UtilityDetail,
    (utitilityDetail) => utitilityDetail.utilityCost,
    {},
  )
  ucDetails: UtilityDetail[];
  //Created and Updated
  @CreateDateColumn({
    name: 'UC_CREATED',
  })
  creatDate: Date;

  @UpdateDateColumn({
    name: 'UC_UPDATED',
  })
  updateDate: Date;
}
