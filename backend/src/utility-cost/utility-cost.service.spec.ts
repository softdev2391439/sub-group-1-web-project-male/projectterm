import { Test, TestingModule } from '@nestjs/testing';
import { UtilityCostService } from './utility-cost.service';

describe('UtilityCostService', () => {
  let service: UtilityCostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UtilityCostService],
    }).compile();

    service = module.get<UtilityCostService>(UtilityCostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
