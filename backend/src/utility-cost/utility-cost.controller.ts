import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UtilityCostService } from './utility-cost.service';
import { CreateUtilityCostDto } from './dto/create-utility-cost.dto';
import { UpdateUtilityCostDto } from './dto/update-utility-cost.dto';

@Controller('utility-cost')
export class UtilityCostController {
  constructor(private readonly utilityCostService: UtilityCostService) {}

  @Post()
  create(@Body() createUtilityCostDto: CreateUtilityCostDto) {
    return this.utilityCostService.create(createUtilityCostDto);
  }

  @Get()
  findAll() {
    return this.utilityCostService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.utilityCostService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUtilityCostDto: UpdateUtilityCostDto,
  ) {
    return this.utilityCostService.update(+id, updateUtilityCostDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.utilityCostService.remove(+id);
  }
}
