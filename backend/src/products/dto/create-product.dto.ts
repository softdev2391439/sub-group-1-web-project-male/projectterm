export class CreateProductDto {
  image: string;

  name: string;

  price: string;

  unit: string;

  category: string;

  subCategorys: string;

  sizes: string;
}
