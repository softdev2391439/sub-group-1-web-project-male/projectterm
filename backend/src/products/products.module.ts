import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { Size } from 'src/sizes/entities/size.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, SubCategory, Size])],
  controllers: [ProductsController],
  providers: [ProductsService],
  exports: [ProductsService],
})
export class ProductsModule {}
