import { Category } from 'src/category/entities/category.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Receiptitem } from 'src/receiptitem/entities/receiptitem.entity';
import { Size } from 'src/sizes/entities/size.entity';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: 'noimage.PNG',
  })
  image: string;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  unit: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Category, (category) => category.products)
  category: Category;

  @ManyToMany(() => SubCategory, (subCategory) => subCategory.products, {
    cascade: true,
  })
  @JoinTable()
  subCategorys: SubCategory[];

  @ManyToMany(() => Size, (size) => size.products, { cascade: true })
  @JoinTable()
  sizes: Size[];

  // @ManyToMany(() => Receipt, (receipt) => receipt.products)
  // receipts: Receipt[];

  @OneToMany(() => Receiptitem, (receiptitem) => receiptitem.product)
  receiptitems: Receiptitem[];
}
