import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SubCategory } from 'src/sub-categorys/entities/sub-category.entity';
import { Size } from 'src/sizes/entities/size.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(SubCategory)
    private subCategorysRepository: Repository<SubCategory>,
    @InjectRepository(Size) private sizesRepository: Repository<Size>,
  ) {}
  async create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.price = parseFloat(createProductDto.price);
    product.unit = parseFloat(createProductDto.unit);
    product.category = JSON.parse(createProductDto.category);
    const a = JSON.parse(createProductDto.subCategorys);
    console.log(a.length);
    if (a.length > 0 && createProductDto.subCategorys) {
      product.subCategorys = JSON.parse(createProductDto.subCategorys);
    }
    if (
      JSON.parse(createProductDto.sizes).length > 0 &&
      createProductDto.sizes
    ) {
      product.sizes = JSON.parse(createProductDto.sizes);
    }
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    return await this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find({
      relations: { category: true, subCategorys: true, sizes: true },
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOneOrFail({
      where: { id: id },
      relations: { category: true, subCategorys: true, sizes: true },
    });
  }

  findByType(TypeId: number) {
    return this.productsRepository.find({
      where: { id: TypeId },
      relations: { category: true, subCategorys: true, sizes: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = new Product();
    product.name = updateProductDto.name;
    product.price = parseFloat(updateProductDto.price);
    product.category = JSON.parse(updateProductDto.category);
    product.unit = parseFloat(updateProductDto.unit);
    const a = JSON.parse(updateProductDto.subCategorys);
    console.log(a.length);
    if (a.length > 0 && updateProductDto.subCategorys) {
      product.subCategorys = JSON.parse(updateProductDto.subCategorys);
      console.log(product.subCategorys);
    }
    if (
      JSON.parse(updateProductDto.sizes).length > 0 &&
      updateProductDto.sizes
    ) {
      product.sizes = JSON.parse(updateProductDto.sizes);
      console.log(product.sizes);
    }
    if (updateProductDto.image && updateProductDto.image !== '') {
      product.image = updateProductDto.image;
    }

    const updateProduct = await this.productsRepository.findOneOrFail({
      where: { id },
      relations: { category: true, subCategorys: true, sizes: true },
    });
    updateProduct.name = product.name;
    updateProduct.price = product.price;
    updateProduct.category = product.category;
    updateProduct.unit = product.unit;
    updateProduct.image = product.image;
    if (updateProduct.subCategorys && updateProduct.subCategorys.length > 0) {
      for (const r of product.subCategorys) {
        const searchSubCategory = updateProduct.subCategorys.find(
          (subCategory) => subCategory.id === r.id,
        );
        if (!searchSubCategory) {
          const newSubCategory = await this.subCategorysRepository.findOneBy({
            id: r.id,
          });
          updateProduct.subCategorys.push(newSubCategory);
        }
      }
      for (let i = 0; i < updateProduct.subCategorys.length; i++) {
        const index = product.subCategorys.findIndex(
          (subCategory) => subCategory.id === updateProduct.subCategorys[i].id,
        );
        if (index < 0) {
          updateProduct.subCategorys.splice(index, 1);
        }
      }
    }
    if (updateProduct.sizes && updateProduct.sizes.length > 0) {
      for (const r of product.sizes) {
        const searchSize = updateProduct.sizes.find((size) => size.id === r.id);
        if (!searchSize) {
          const newSize = await this.sizesRepository.findOneBy({ id: r.id });
          updateProduct.sizes.push(newSize);
        }
      }
      for (let i = 0; i < updateProduct.sizes.length; i++) {
        const index = product.sizes.findIndex(
          (role) => role.id === updateProduct.sizes[i].id,
        );
        if (index < 0) {
          updateProduct.sizes.splice(index, 1);
        }
      }
    }
    console.log(updateProduct);
    await this.productsRepository.save(updateProduct);
    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { category: true, subCategorys: true, sizes: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.productsRepository.findOneOrFail({
      where: { id },
    });
    await this.productsRepository.remove(deleteUser);

    return deleteUser;
  }
}
