import { Injectable } from '@nestjs/common';
import { CreateSubCategoryDto } from './dto/create-sub-category.dto';
import { UpdateSubCategoryDto } from './dto/update-sub-category.dto';
import { SubCategory } from './entities/sub-category.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SubCategorysService {
  constructor(
    @InjectRepository(SubCategory)
    private subCategorysRepository: Repository<SubCategory>,
  ) {}
  create(createSubCategoryDto: CreateSubCategoryDto) {
    return this.subCategorysRepository.save(createSubCategoryDto);
  }

  findAll() {
    return this.subCategorysRepository.find();
  }

  findOne(id: number) {
    return this.subCategorysRepository.findOneByOrFail({ id: id });
  }

  async update(id: number, updateSubCategoryDto: UpdateSubCategoryDto) {
    await this.subCategorysRepository.findOneByOrFail({ id });
    await this.subCategorysRepository.update(id, updateSubCategoryDto);
    const updatedSubCategory = await this.subCategorysRepository.findOneBy({
      id,
    });
    return updatedSubCategory;
  }

  async remove(id: number) {
    const deletesubCategory = await this.subCategorysRepository.findOneOrFail({
      where: { id },
    });
    await this.subCategorysRepository.remove(deletesubCategory);

    return deletesubCategory;
  }
}
