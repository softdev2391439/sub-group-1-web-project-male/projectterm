import { Module } from '@nestjs/common';
import { SubCategorysService } from './sub-categorys.service';
import { SubCategorysController } from './sub-categorys.controller';
import { SubCategory } from './entities/sub-category.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([SubCategory])],
  controllers: [SubCategorysController],
  providers: [SubCategorysService],
})
export class SubCategorysModule {}
