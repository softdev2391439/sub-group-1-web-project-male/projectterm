import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { SubCategorysService } from './sub-categorys.service';
import { CreateSubCategoryDto } from './dto/create-sub-category.dto';
import { UpdateSubCategoryDto } from './dto/update-sub-category.dto';

@Controller('sub-categorys')
export class SubCategorysController {
  constructor(private readonly subCategorysService: SubCategorysService) {}

  @Post()
  create(@Body() createSubCategoryDto: CreateSubCategoryDto) {
    return this.subCategorysService.create(createSubCategoryDto);
  }

  @Get()
  findAll() {
    return this.subCategorysService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.subCategorysService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSubCategoryDto: UpdateSubCategoryDto,
  ) {
    return this.subCategorysService.update(+id, updateSubCategoryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.subCategorysService.remove(+id);
  }
}
