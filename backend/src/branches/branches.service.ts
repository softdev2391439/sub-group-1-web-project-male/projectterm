import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Repository } from 'typeorm';
import { Branch } from './entities/branch.entity';

@Injectable()
export class BranchesService {
  constructor(
    @InjectRepository(Branch) private branchesRepository: Repository<Branch>,
  ) {}
  async create(createBranchDto: CreateBranchDto) {
    return await this.branchesRepository.save(createBranchDto);
  }

  async findAll() {
    return await this.branchesRepository.find();
  }

  findOne(id: number) {
    return this.branchesRepository.findOneByOrFail({ id });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    await this.branchesRepository.findOneByOrFail({ id });
    await this.branchesRepository.update(id, updateBranchDto);
    return await this.branchesRepository.findOneByOrFail({ id });
  }

  async remove(id: number) {
    const removeBranch = await this.branchesRepository.findOneByOrFail({ id });
    await this.branchesRepository.remove(removeBranch);
    return removeBranch;
  }
}
