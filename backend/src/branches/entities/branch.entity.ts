import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';

@Entity({
  name: 'BRANCH',
})
export class Branch {
  //Info
  @PrimaryGeneratedColumn({ name: 'BRANCH_ID' })
  id: number;
  @Column({
    name: 'BRANCH_NAME',
  })
  name: string;
  @Column({
    name: 'BRANCH_ADDRESS',
  })
  address: string;
  @Column({
    name: 'BRANCH_CITY',
  })
  city: string;
  @Column({
    name: 'BRANCH_PROVINCE',
  })
  province: string;
  @Column({
    name: 'BRANCH_COUNTRY',
  })
  country: string;
  @Column({
    name: 'BRANCH_POSTAL_CODE',
  })
  postalcode: string;
  @Column({
    name: 'BRANCH_LATITUDE',
    type: 'decimal',
    precision: 30,
    scale: 20,
  })
  latitude: number;
  @Column({
    name: 'BRANCH_LONGITUDE',
    type: 'decimal',
    precision: 30,
    scale: 20,
  })
  longitude: number;

  @OneToMany(() => User, (user) => user.branch)
  users: User[];

  //Cretated and Updated
  @CreateDateColumn({
    name: 'BRANCH_CREATED',
  })
  created: Date;
  @UpdateDateColumn({
    name: 'BRANCH_UPDATED',
  })
  updated: Date;
}
