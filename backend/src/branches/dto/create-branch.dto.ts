export class CreateBranchDto {
  name: string;
  address: string;
  city: string;
  province: string;
  country: string;
  postalcode: string;
  latitude: number;
  longitude: number;
}
