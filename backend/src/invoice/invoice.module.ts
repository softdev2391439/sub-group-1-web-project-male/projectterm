import { Module } from '@nestjs/common';
import { InvoiceService } from './invoice.service';
import { InvoiceController } from './invoice.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { InvoiceDetail } from 'src/invoice-detail/entities/invoice-detail.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Invoice, InvoiceDetail, User, Branch])],
  controllers: [InvoiceController],
  providers: [InvoiceService],
})
export class InvoiceModule {}
