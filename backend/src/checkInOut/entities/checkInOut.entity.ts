import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class CheckInOut {
  @PrimaryGeneratedColumn({
    name: 'CHECK_ID',
  })
  id: number;

  @Column({
    name: 'CHECK_NAME',
  })
  name: string;

  @Column({
    name: 'CHECK_PASSWORD',
  })
  password: string;

  @Column({
    name: 'CHECK_TIME_IN',
  })
  timeIn: Date;

  @Column({
    name: 'CHECK_TIME_OUT',
  })
  timeOut: Date;

  @Column({
    name: 'CHECK_STATUS',
  })
  status: string;

  @Column({
    name: 'CHECK_TOTAL_DURATION',
  })
  totalDuration: number;

  @Column({
    name: 'CHECK_DATE',
  })
  date: Date;
}
