import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckInOutDto } from './create-checkInOut.dto';

export class UpdateCheckInOutDto extends PartialType(CreateCheckInOutDto) {}
