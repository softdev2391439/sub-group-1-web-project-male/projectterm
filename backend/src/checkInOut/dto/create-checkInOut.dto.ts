export class CreateCheckInOutDto {
  name: string;
  password: string;
  timeIn: Date;
  timeOut: Date;
  status: string;
  totalDuration: number;
}
