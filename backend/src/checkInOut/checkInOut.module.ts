import { Module } from '@nestjs/common';
import { CheckInOutService } from './checkInOut.service';
import { CheckInOutController } from './checkInOut.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckInOut } from './entities/checkInOut.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckInOut])],
  controllers: [CheckInOutController],
  providers: [CheckInOutService],
})
export class CheckInOutModule {}
