import { Injectable } from '@nestjs/common';
import { CreateCheckInOutDto } from './dto/create-checkInOut.dto';
import { UpdateCheckInOutDto } from './dto/update-checkInOut.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckInOut } from './entities/checkInOut.entity';
import { Repository } from 'typeorm/repository/Repository';

@Injectable()
export class CheckInOutService {
  constructor(
    @InjectRepository(CheckInOut)
    private checkInOutRepository: Repository<CheckInOut>,
  ) {}
  create(createCheckInOutDto: CreateCheckInOutDto) {
    return this.checkInOutRepository.save(createCheckInOutDto);
  }

  findAll(): Promise<CheckInOut[]> {
    return this.checkInOutRepository.find();
  }

  findOne(id: number) {
    return this.checkInOutRepository.findOneBy({ id: id });
  }

  async update(id: number, updateCheckInOutDto: UpdateCheckInOutDto) {
    await this.checkInOutRepository.update(id, updateCheckInOutDto);
    const checkInout = await this.checkInOutRepository.findOneBy({ id });
    return checkInout;
  }

  async remove(id: number) {
    const deletecheckInOut = await this.checkInOutRepository.findOneBy({ id });
    return this.checkInOutRepository.remove(deletecheckInOut);
  }
}
