import { Module } from '@nestjs/common';
import { StockService } from './stock.service';
import { StockController } from './stock.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { StockDetail } from 'src/stockDetails/entities/stockDetail.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branches/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, StockDetail, User, Branch])],
  controllers: [StockController],
  providers: [StockService],
  exports: [StockService],
})
export class stocksModule {}
