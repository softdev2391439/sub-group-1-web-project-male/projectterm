import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto): Promise<Customer> {
    return this.customersRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customersRepository.find();
  }

  findOne(id: number) {
    return this.customersRepository.findOneBy({ id: id });
  }

  findOneByTel(tel: string) {
    return this.customersRepository.findOneBy({ tel: tel });
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    await this.customersRepository.update(id, updateCustomerDto);
    const customer = await this.customersRepository.findOneBy({ id });
    return customer;
  }

  async remove(id: number) {
    const deleteCustomer = await this.customersRepository.findOneBy({ id });
    await this.customersRepository.remove(deleteCustomer);
  }
}
