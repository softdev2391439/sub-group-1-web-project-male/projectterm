import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn({
    name: 'CUS_ID',
  })
  id: number;

  @Column({
    name: 'CUS_NAME',
  })
  name: string;

  @Column({
    name: 'CUS_TEL',
  })
  tel: string;

  @Column({
    name: 'CUS_GENDER',
  })
  gender: string;

  @Column({
    name: 'CUS_INDATE',
    type: 'date',
  })
  inDate: Date;

  @Column({
    name: 'CUS_POINT_AMOUNT',
  })
  pointAmount: number;

  @Column({
    name: 'CUS_POINT_RATE',
  })
  pointRate: number;

  // promotion
  @OneToMany(() => Promotion, (promotion) => promotion.customer)
  promotions: Promotion[];

  // recipt
  // @OneToMany(() => Receipt, (receipt) => receipt.customer)
  // receipts: Receipt[];

  // for Receipt
  // @ManyToOne(() => Customer, (customer) => customer.receipts)
  // customer: Customer;

  @OneToMany(() => Receipt, (receipt) => receipt.customer)
  receipts: Receipt[];
}
