export class CreateCustomerDto {
  name: string;

  tel: string;

  gender: string;

  inDate: Date;

  pointAmount: number;

  pointRate: number;
}
