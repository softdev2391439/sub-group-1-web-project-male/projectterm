export class CreatePromotionDto {
  name: string;
  details: string;
  startDate: Date;
  endDate: Date;
  discount: number;
  pointUse: number;
  status: 'Active' | 'Inactive';
}
