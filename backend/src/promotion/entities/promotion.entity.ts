import { Customer } from 'src/customers/entities/customer.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  details: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  discount: number;

  @Column()
  status: 'Active' | 'Inactive';

  @Column()
  pointUse: number;

  @ManyToOne(() => Customer, (customer) => customer.promotions)
  customer: Customer;

  @ManyToMany(() => Receipt, (receipt) => receipt.promotions)
  receipts: Receipt[];
}
