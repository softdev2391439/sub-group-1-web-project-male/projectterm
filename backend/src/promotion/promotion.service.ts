import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    return this.promotionRepository.save(createPromotionDto);
  }

  findAll(): Promise<Promotion[]> {
    return this.promotionRepository.find();
  }

  findOne(id: number) {
    return this.promotionRepository.findOneBy({ id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    await this.promotionRepository.update(id, updatePromotionDto);
    const promotions = await this.promotionRepository.findOneBy({ id });
    return promotions;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionRepository.findOneBy({ id });
    return this.promotionRepository.remove(deletePromotion);
  }
}
