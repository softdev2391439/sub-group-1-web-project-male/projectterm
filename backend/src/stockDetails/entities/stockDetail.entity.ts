import { Material } from 'src/material/entities/material.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Column,
  Double,
  JoinColumn,
} from 'typeorm';

@Entity()
export class StockDetail {
  @PrimaryGeneratedColumn()
  SD_ID: number;

  @Column({ type: 'real' })
  SD_QOH: Double;

  @Column({ type: 'real' })
  SD_BALANCE: Double;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Material)
  @JoinColumn({ name: 'M_ID' })
  SD_MATERIAL: Material;

  @ManyToOne(() => Stock, (stock) => stock.S_STOCKDETAILS)
  SD_STOCK: Stock;
}
