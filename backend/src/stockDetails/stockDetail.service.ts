import { Injectable } from '@nestjs/common';
import { CreateStockDetailDto } from './dto/create-stockDetail.dto';
import { UpdateStockDetailDto } from './dto/update-stockDetail.dto';
import { Repository } from 'typeorm';
import { StockDetail } from './entities/stockDetail.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { Material } from 'src/material/entities/material.entity';

@Injectable()
export class StockDetailService {
  constructor(
    @InjectRepository(StockDetail)
    private stockDetailsRepository: Repository<StockDetail>,
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createUserDto: CreateStockDetailDto) {
    const sd = new StockDetail();
    sd.SD_QOH = createUserDto.SD_QOH;
    sd.SD_BALANCE = createUserDto.SD_BALANCE;
    if (createUserDto.SD_MATERIAL && createUserDto.SD_MATERIAL > 0) {
      const material = await this.materialsRepository.findOneOrFail({
        where: { M_ID: createUserDto.SD_MATERIAL },
      });
      sd.SD_MATERIAL = material;
    }

    return this.stockDetailsRepository.save(sd);
  }

  findAll() {
    return this.stockDetailsRepository.find({
      relations: { SD_MATERIAL: true },
    });
  }

  findOne(id: number) {
    return this.stockDetailsRepository.findOne({
      where: { SD_ID: id },
      relations: { SD_MATERIAL: true },
    });
  }

  findByStockId(stockId: number) {
    return this.stockDetailsRepository.find({
      where: { SD_STOCK: { S_ID: stockId } },
      relations: { SD_MATERIAL: true },
    });
  }

  async update(id: number, updateStockDto: UpdateStockDetailDto) {
    await this.stockDetailsRepository.findOneByOrFail({ SD_ID: id });
    const sd = new StockDetail();
    sd.SD_ID = id;
    sd.SD_QOH = updateStockDto.SD_QOH;
    sd.SD_BALANCE = updateStockDto.SD_BALANCE;
    if (updateStockDto.SD_MATERIAL && updateStockDto.SD_MATERIAL > 0) {
      const material = await this.materialsRepository.findOneOrFail({
        where: { M_ID: updateStockDto.SD_MATERIAL },
      });
      sd.SD_MATERIAL = material;
    }

    await this.stockDetailsRepository.update(id, sd);
    const updatedStockDetail = await this.stockDetailsRepository.findOneBy({
      SD_ID: id,
    });
    return updatedStockDetail;
  }

  async remove(id: number) {
    const deleteStockDetail = await this.stockDetailsRepository.findOneOrFail({
      where: { SD_ID: id },
    });
    await this.stockDetailsRepository.remove(deleteStockDetail);

    return deleteStockDetail;
  }
}
