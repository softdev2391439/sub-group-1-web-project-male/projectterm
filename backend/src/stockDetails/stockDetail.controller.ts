import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StockDetailService } from './stockDetail.service';
import { CreateStockDetailDto } from './dto/create-stockDetail.dto';
import { UpdateStockDetailDto } from './dto/update-stockDetail.dto';
//import { AuthGuard } from 'src/auth/auth.guard';

//@UseGuards(AuthGuard)
@Controller('stockDetails')
export class StockDetailController {
  constructor(private readonly stockService: StockDetailService) {}

  @Get()
  findAll() {
    return this.stockService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockService.findOne(+id);
  }

  @Get('stockId/:stockId')
  findByStockId(@Param('stockId') stockId: string) {
    return this.stockService.findByStockId(+stockId);
  }

  @Post()
  addNew(@Body() createStockDto: CreateStockDetailDto) {
    return this.stockService.create(createStockDto);
  }
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockDto: UpdateStockDetailDto,
  ) {
    return this.stockService.update(+id, updateStockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockService.remove(+id);
  }
}
