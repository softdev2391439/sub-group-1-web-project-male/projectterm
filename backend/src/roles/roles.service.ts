import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createRoleDto: CreateRoleDto) {
    return await this.rolesRepository.save(createRoleDto);
  }

  findAll() {
    return this.rolesRepository.find();
  }

  async findOne(id: number) {
    return await this.rolesRepository.findOneBy({ id });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    await this.rolesRepository.findOneByOrFail({ id });
    await this.rolesRepository.update(id, updateRoleDto);
    return await this.rolesRepository.findOneBy({ id });
  }

  async remove(id: number) {
    const removeRole = await this.rolesRepository.findOneByOrFail({ id });
    await this.rolesRepository.remove(removeRole);
    return removeRole;
  }
}
